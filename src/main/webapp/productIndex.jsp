<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Product</title>
</head>
<body>
<h1><%= "Products Section" %>
</h1>
<br/>
<a href="products.jsp"  >Show Products</a><br>
<a href="saveProduct.jsp">Save</a><br>
<a href="deleteProduct.jsp">Delete</a><br>
<a href="updateProduct.jsp">Update</a><br>
</body>
</html>