package com.example.ecommerce.servlets.product;

import com.example.ecommerce.models.Product;
import com.example.ecommerce.services.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "DeleteProductServlet", value = "/DeleteProductServlet")
public class DeleteProductServlet  extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        ProductService productService = new ProductService();

        String Name= req.getParameter("name");
        Product pName = new Product();
        pName.setName(Name);
        int status = productService.delete(pName);

        if (status == 0) {
            RequestDispatcher request = req.getRequestDispatcher("success.jsp");
            request.forward(req, resp);
        } else if (status == 2) {
            RequestDispatcher request = req.getRequestDispatcher("deleteProduct.jsp");
            request.include(req, resp);
            out.print("<br><center>Invalid Product name!!</center>");
        } else if (status == 1) {
            RequestDispatcher request = req.getRequestDispatcher("deleteProduct.jsp");
            request.include(req, resp);
            out.print("<br><center>Product doesn't exist</center>");
        }

    }
}
