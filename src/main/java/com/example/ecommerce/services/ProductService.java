package com.example.ecommerce.services;

import com.example.ecommerce.dao.CategoryDao;
import com.example.ecommerce.dao.ProductDao;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Page;
import com.example.ecommerce.models.Product;

import java.util.List;

public class ProductService implements Service<Product> {

    private final ProductDao pDao;
    private CategoryDao cDao;


    public ProductService() {
        this.pDao = new ProductDao();
    }

    @Override
    public Product get(String name) {
        return pDao.get(name);
    }

    @Override
    public List<Product> getAll() {
        return pDao.getAll();
    }

    @Override
    public int save(Product product) {
        boolean validate = pDao.validate(product);

        if (validate) {
            pDao.save(product);
            for (Category c : product.getCategories())
                pDao.linkProduct(c, product);
        } else {
            return 2;
        }
        return 0;
    }

    @Override
    public int update(Product product, Product product1) {

        boolean validate= pDao.validate(product);
        boolean validate1= pDao.validate(product1);
        if(validate==true && validate1==true) {
            int status = pDao.check(product);
            if (status == 1) {
                pDao.update(product, product1);
            }
            else{
                return 0;
            }
        }
        else{
            return 2;
        }
        return 1;
    }



    @Override
    public int delete(Product product) {
        boolean validate = pDao.validate(product);
        if (validate) {
            int status = pDao.check(product);
            if (status == 1) {
                pDao.delete(product);
            } else {
                return 1;
            }
        } else {
            return 2;
        }
        return 0;
    }


    public Page<Product> getPage(int pageNumber, int pageSize) {
        Page<Product> page = new Page<>(pageNumber, pageSize);
        page.setRecords(pDao.getPage(page));
        page.setTotal(pDao.count());
        return page;
    }
/*
    public List<Product> getAllByCategory(Category c) {
        return pDao.getAllByCategory(c);
    }

    public List<Category> getCategories(Product p) {
        return pDao.getCategories(p);

    }
 */

}