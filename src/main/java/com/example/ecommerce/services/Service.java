package com.example.ecommerce.services;


import java.util.List;

public  interface Service<T> {

    public  T get(String name);

    public  List<T> getAll();

    public  int save(T t);

    public  int update(T t,T t1);

    public  int delete(T t);
}