<!DOCTYPE html>
<html>
<head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 20px;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
}

li a {a
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover {
  background-color: #111;
}
</style>
</head>
<body>

<ul>
  <li><a class="active" href="#home">E-commerce</a></li>
  <li><a href="productIndex.jsp">Products</a></li>
  <li><a href="categoryIndex.jsp">Categories</a></li>
  <li><a href="#about">About</a></li>
</ul>

</body>
</html>
