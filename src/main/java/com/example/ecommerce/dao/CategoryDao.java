package com.example.ecommerce.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.example.ecommerce.database.Database;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Page;
import com.example.ecommerce.models.Product;

public class CategoryDao implements Dao<Category> {
    private Connection conn;

    public CategoryDao() {
        this.conn = Database.getConnection();
    }

    public void linkProduct(Category c, Product p) {
        if (c == null || p == null)
            return;
        try {
            PreparedStatement pst = conn
                    .prepareStatement("INSERT IGNORE INTO product_category (product_id, category_id) VALUES(?,?)");
            pst.setInt(1, p.getId());
            pst.setInt(2, c.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while linking product. Category ID " + c.getId() + ". Error: " + e.getMessage());
        }
    }

    public int count() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(id) FROM category");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting count of category. Error: " + e.getMessage());
        }
        return 0;
    }

    @Override
    public Category get(String name) {
        try {
            PreparedStatement st = conn.prepareStatement("SELECT id, name FROM category WHERE name=?");
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                return c;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting category. Category ID " + name + ". Error: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<Category> getAll() {
        List<Category> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM category");
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting category list. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public void save(Category c) {
        if (c == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT IGNORE INTO category (name) VALUES(?)");
            pst.setString(1, c.getName());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("SQL Error while adding a category, Category ID " + c.getId());
        }
    }

    @Override
    public void delete(Category c) {
        if (c == null)
            return;
        try {
            PreparedStatement st = conn.prepareStatement("DELETE FROM product_category where category_id=?");
            st.setInt(1, c.getId());
            st.executeUpdate();
            PreparedStatement st2 = conn.prepareStatement("DELETE FROM category where id=?");
            st2.setInt(1, c.getId());
            st2.executeUpdate();
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while deleting a category, Category ID " + c.getId() + ". Error: " + e.getMessage());
        }
    }

    @Override
    public void update(Category oldCategory, Category newCategory) {
        try {
            if (!oldCategory.getName().equals(newCategory.getName())) {
                PreparedStatement st = conn.prepareStatement("DELETE FROM product_category where category_id=?");
                st.setInt(1, oldCategory.getId());
                st.executeUpdate();
            }
            PreparedStatement pst = conn.prepareStatement("UPDATE category SET name=? where name=?");
            pst.setString(1, newCategory.getName());
            pst.setString(2, oldCategory.getName());
            pst.executeUpdate();

        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while updating a category " + e.getMessage());
        }
    }

    public List<Category> getPage(Page page) {
        List<Category> result = new ArrayList<>();
        try {
            PreparedStatement pt = conn.prepareStatement("SELECT * FROM category LIMIT ? OFFSET ?");
            pt.setInt(1, page.getPageSize());
            pt.setInt(2, (page.getPageNumber() - 1) * page.getPageSize());
            ResultSet rs = pt.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while paging products. Error: " + e.getMessage());
        }
        return result;
    }

    public int check(Category category) {
        int status = 0;
        try {
            PreparedStatement pstmt = conn.prepareStatement("select * from category where name=?");
            pstmt.setString(1, category.getName());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                status = 1;
                category.setId(rs.getInt(1));
            }
        } catch (SQLException s) {
            System.out.println("check failed");
        }
        return status;
    }

    public boolean validate(Category category) {
        String string = category.getName();
        String pattern = "[a-zA-Z]+[0-9-_()]*";
        if (!string.matches(pattern)) {
            return false;
        }
        return true;
    }

}

