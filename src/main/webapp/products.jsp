<%@ page import="com.example.ecommerce.services.ProductService" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.ecommerce.models.Product" %>
<%@ page import="com.example.ecommerce.models.Page" %>
<%@ page import="com.example.ecommerce.models.Category"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Products</title>
</head>
<body>
<div class="container">
    <%
        ProductService productService = new ProductService();
        int currPageNum = 1;
        try {
            currPageNum = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
        }
        Page<Product> currPage = productService.getPage(currPageNum, 3);
        List<Product> products = currPage.getRecords();
    %>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Mfg Date</th>
            <th scope="col">Exp Date</th>
            <th scope="col">Categories</th>

        </tr>
        </thead>
        <tbody>
        <%
            for (Product product : products) {
                out.println("<tr>");
                out.println(String.format("<td scope=\"row\">%d</td>", product.getId()));
                out.println(String.format("<td>%s</td>", product.getName()));
                out.println(String.format("<td>%s</td>", product.getMfgDate()));
                out.println(String.format("<td>%s</td>", product.getExpDate()));
                %>
                <td><%
                List<Category> categories = product.getCategories();

                    if(categories!=null){

                     for(int i=0;i<categories.size();i++){
                                if(i==(categories.size()-1)){
                                          out.print(categories.get(i).getName()+".");
                                                   }
                                                    else{
                                                  out.print(categories.get(i).getName()+", ");
                                                  }
                                                  }
                    }


                        %>
                 </td>
                 <%
                out.println("</tr>");
            }
        %>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination float-right">
            <%
                long totalPages = currPage.getTotalPages();

                if (currPageNum != 1)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">Previous</a></li>", currPageNum - 1));
                for (int i = 0; i < totalPages; i++) {
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">%d</a></li>", i+1, i + 1));
                }
                if (currPageNum != totalPages)
                    out.println(String.format("<li class=\"page-item\"><a class=\"page-link\" href=\"?page=%d\">Next</a></li>", currPageNum + 1));
            %>

        </ul>
    </nav>

</div>
</body>
</html>
