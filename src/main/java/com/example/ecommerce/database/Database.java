package com.example.ecommerce.database;

import java.sql.*;


public class Database {
    private static String url = "jdbc:mysql://localhost:3306/";
    private static String db = "ecommerce";
    private static String userName = "root";
    private static String password = "";
    protected static Connection conn;


    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(url + db, userName, password);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Database connection error");
        }

    }
}
