package com.example.ecommerce.services;

import com.example.ecommerce.dao.CategoryDao;
import com.example.ecommerce.dao.ProductDao;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Page;
import com.example.ecommerce.models.Product;


import java.util.List;

public class CategoryService implements Service<Category> {

    private CategoryDao cDao;


    public CategoryService() {
        this.cDao = new CategoryDao();
    }

    @Override
    public Category get(String name) {
        return cDao.get(name);
    }

    @Override
    public List<Category> getAll() {
        return cDao.getAll();
    }

    @Override
    public int save(Category category) {
        boolean validate = cDao.validate(category);
        if (validate) {
            int status = cDao.check(category);
            if (status == 0) {
                cDao.save(category);
            } else {
                return 1;
            }
        } else {
            return 2;
        }
        return 0;
    }

    @Override
    public int update(Category category, Category category1) {
        boolean validate = cDao.validate(category);
        boolean validate1 = cDao.validate(category1);
        if (validate == true && validate1 == true) {
            int status = cDao.check(category);
            if (status == 1) {
                cDao.update(category, category1);
            } else {
                return 1;
            }
        } else {
            return 2;
        }
        return 0;
    }


    @Override
    public int delete(Category category) {
        boolean validate = cDao.validate(category);
        if (validate) {
            int status = cDao.check(category);
            if (status == 1) {
                cDao.delete(category);
            } else {
                return 1;
            }
        } else {
            return 2;
        }
        return 0;
    }

    public Page<Category> getPage(int pageNumber, int pageSize) {
        Page<Category> page = new Page<>(pageNumber, pageSize);
        page.setRecords(cDao.getPage(page));
        page.setTotal(cDao.count());
        return page;
    }
}
