package com.example.ecommerce.dao;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.example.ecommerce.database.Database;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Page;
import com.example.ecommerce.models.Product;

public class ProductDao implements Dao<Product> {
    private Connection conn;


    public ProductDao() {
        this.conn = Database.getConnection();
    }

    public void linkProduct(Category c, Product p) {
        if (c == null || p == null)
            return;
        try {
            Product pr = get(p.getName());
            PreparedStatement pst = conn
                    .prepareStatement("INSERT IGNORE INTO product_category (product_id, category_id) VALUES(?,?)");
            pst.setInt(1, pr.getId());
            pst.setInt(2, c.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while linking product. Category ID " + c.getId() + ". Error: " + e.getMessage());
        }
    }

    public int count() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(id) FROM product");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting count of products. Error: " + e.getMessage());
        }
        return 0;
    }

    public List<Category> getCategories(Product p) {
        List<Category> result = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement("SELECT c.name FROM category c " +
                    "inner join product_category on c.id=product_category.category_id inner join " +
                    "product on product.id=product_category.product_id and product.name=?");
            st.setString(1, p.getName());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getString(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting categories by product's id. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public Product get(String name) {
        try {
            PreparedStatement st = conn.prepareStatement("SELECT id, name, mfg_date, exp_date FROM product WHERE name=?");
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                return p;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting product.Product ID " + name + ". Error: " + e.getMessage());
        }
        return null;
    }

    public List<Product> getAllByCategory(Category c) {
        List<Product> result = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement("SELECT p.id, p.name, p.mfg_date, p.exp_date FROM product p " +
                    "inner join product_category on p.id=product_category.product_id inner join " +
                    "category on category.id=product_category.category_id and category.name=?");
            st.setString(1, c.getName());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                result.add(p);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list by category. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public List<Product> getAll() {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM product");
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                p.setCategories(getCategories(p));
                result.add(p);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public void save(Product p) {
        if (p == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT INTO product (name, mfg_date, exp_date) VALUES(?,?,?)");
            pst.setString(1, p.getName());
            pst.setDate(2, p.getMfgDate());
            pst.setDate(3, p.getExpDate());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("SQL Error while adding a product, Product ID " + p.getId());
        }
    }

    @Override
    public void delete(Product p) {
        if (p == null)
            return;
        try {
            PreparedStatement st = conn.prepareStatement("DELETE FROM product_category where product_id=?");
            st.setInt(1, p.getId());
            st.executeUpdate();
            PreparedStatement st2 = conn.prepareStatement("DELETE FROM product where id=?");
            st2.setInt(1, p.getId());
            st2.executeUpdate();

        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while deleting product, Product ID " + p.getId() + ". Error: " + e.getMessage());
        }
    }

    @Override
    public void update(Product p, Product p1) {
        if (p == null)
            return;
        try {
            PreparedStatement pst = conn
                    .prepareStatement("UPDATE product SET name=? ,mfg_date=? ,exp_date=? where name=?");
            pst.setString(1, p1.getName());
            pst.setDate(2, p1.getMfgDate());
            pst.setDate(3, p1.getExpDate());
            pst.setString(4, p.getName());
            pst.executeUpdate();

        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while updating a product, Product ID " + p.getId() + ". Error: " + e.getMessage());
        }
    }

    public List<Product> getPage(Page page) {
        List<Product> result = new ArrayList<>();
        try {
            PreparedStatement pt = conn.prepareStatement("SELECT * FROM product LIMIT ? OFFSET ?");
            pt.setInt(1, page.getPageSize());
            pt.setInt(2, (page.getPageNumber() - 1) * page.getPageSize());
            ResultSet rs = pt.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                p.setCategories(getCategories(p));
                result.add(p);
            }
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while paging products. Error: " + e.getMessage());
        }
        return result;
    }

    public boolean validate(Product product) {
        String string = product.getName();
        String pattern = "[a-zA-Z]+[0-9-_()]*";
        if (!string.matches(pattern)) {
            return false;
        }
        return true;
    }
    public int check(Product product) {
        int status = 0;
        try {
            PreparedStatement pstmt = conn.prepareStatement("select * from product where name=?");
            pstmt.setString(1, product.getName());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                status = 1;
                product.setId(rs.getInt(1));
            }
        } catch (SQLException s) {
            System.out.println("check failed");
        }
        return status;
    }

}
