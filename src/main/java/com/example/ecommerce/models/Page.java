package com.example.ecommerce.models;

import java.util.List;

public class Page<T> {

    private int pageNumber, pageSize;
    private long total;
    private List<T> records;


    public Page(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public List<T> getRecords() {
        return records;
    }

    public long getTotal() {
        return total;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public long getTotalPages() {
        long totalPages = total / pageSize;
        return total % pageSize == 0 ? totalPages : totalPages + 1;
    }
}
