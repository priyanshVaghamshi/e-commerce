package com.example.ecommerce.dao;

import java.util.List;

public interface Dao<T> {

    T get(String name);

    List<T> getAll();

    void save(T t);

    void delete(T t);

    void update(T t,T t1);


}
