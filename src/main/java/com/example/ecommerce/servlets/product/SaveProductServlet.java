package com.example.ecommerce.servlets.product;

import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Product;
import com.example.ecommerce.services.ProductService;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "SaveProductServlet", value = "/SaveProductServlet")

public class SaveProductServlet extends HttpServlet {


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        ProductService productService = new ProductService();

        String name = req.getParameter("product_name");
        String mfg_date = req.getParameter("mfg_date");
        String exp_date = req.getParameter("exp_date");
        String[] categories = req.getParameterValues("categories");


        if (categories==null  || exp_date.compareTo(mfg_date) < 0) {
            RequestDispatcher request = req.getRequestDispatcher("saveProduct.jsp");
            request.include(req, resp);
            out.print("<br><center>check atleast one category Or Date invalid relation </center>");
        } else {
            List<Category> categoryList = new ArrayList<>();
            for (String id : categories) {
                Category c = new Category("");
                c.setId(Integer.parseInt(id));
                categoryList.add(c);
            }

            Product p = new Product();
            p.setName(name);
            p.setMfgDate(Date.valueOf(mfg_date));
            p.setExpDate(Date.valueOf(exp_date));
            p.setCategories(categoryList);
            productService.save(p);

            RequestDispatcher request = req.getRequestDispatcher("success.jsp");
            request.forward(req, resp);
        }
    }
}
