package com.example.ecommerce.servlets.product;

import com.example.ecommerce.models.Product;
import com.example.ecommerce.services.ProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

@WebServlet("/servlets.product.UpdateProductServlet")
public class UpdateProductServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        ProductService productService = new ProductService();

        String oldName= req.getParameter("old_name");
        String newName= req.getParameter("new_name");
        String mfg_date =req.getParameter("mfg_date");
        String exp_date =req.getParameter("exp_date");

        if(oldName.isEmpty()){
            out.print("<center>Invalid name or please enter Old Name properly</center>");
            RequestDispatcher request = req.getRequestDispatcher("updateProduct.jsp");
            request.include(req, resp);
        }
        else{
            Product p = new Product();
            Product p1 = new Product();
            p1=productService.get(oldName);
            p.setName(oldName);
            if(!newName.isEmpty()) p1.setName(newName);
            if(!mfg_date.isEmpty()) p1.setMfgDate(Date.valueOf(mfg_date));
            if(!exp_date.isEmpty()) p1.setExpDate(Date.valueOf(exp_date));
            productService.update(p,p1);

            RequestDispatcher request = req.getRequestDispatcher("success.jsp");
            request.forward(req, resp);

        }


    }
}
