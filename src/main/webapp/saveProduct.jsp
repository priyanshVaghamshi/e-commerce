<%@ page import="com.example.ecommerce.services.CategoryService" %>
<%@ page import="com.example.ecommerce.models.Category" %>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Product Save</title>
    </head>
<body>
<center>
    <h1>Save a Product Form</h1>
    <form action="SaveProductServlet" method="post">
    			<table style="with: 50%" >
    				<tr>
    					<td>Name*</td>
    					<td><input type="text" name="product_name"/required></td>
    				</tr>
    				<tr>
    					<td>Mfg_Date*</td>
    					<td><input type="date" name="mfg_date"/required></td>
    				</tr>
    				<tr>
    					<td>Exp_Date*</td>
    					<td><input type="date" name="exp_date"/required></td>
    				</tr>
    			</table>
    			<br>
    			Check the categories for a given product<br>

                    <%CategoryService cService = new CategoryService();
                    List<Category> categories = cService.getAll();
                     for (Category c : categories) {%>
                                    <input type="checkbox" name="categories" value="<%= c.getId() %>"><%= c.getName() %>
                                    <br>
                                            <% } %><br>
    			<input type="submit" value="Save" /></form></center>
</body>
</html>