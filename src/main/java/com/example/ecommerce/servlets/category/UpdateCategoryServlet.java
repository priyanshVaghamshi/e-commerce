package com.example.ecommerce.servlets.category;

import com.example.ecommerce.models.Category;
import com.example.ecommerce.services.CategoryService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UpdateCategoryServlet", value = "/UpdateCategoryServlet")
public class UpdateCategoryServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        CategoryService categoryService = new CategoryService();

        String oldName = req.getParameter("old_name");
        String newName = req.getParameter("new_name");
        Category cOldName = new Category(oldName);
        Category cNewName = new Category(newName);
        int status = categoryService.update(cOldName, cNewName);


        if (status == 0) {
            RequestDispatcher request = req.getRequestDispatcher("success.jsp");
            request.forward(req, resp);
        } else if (status == 2) {
            RequestDispatcher request = req.getRequestDispatcher("updateCategory.jsp");
            request.include(req, resp);
            out.print("<br><center>Invalid Category name!!</center>");
        } else if (status == 1) {
            RequestDispatcher request = req.getRequestDispatcher("updateCategory.jsp");
            request.include(req, resp);
            out.print("<br><center>Category doesn't exist</center>");
        }


    }
}
