package com.example.ecommerce.models;

import java.sql.Date;
import java.util.List;

public class Product {

    private int id;
    private String name;
    private Date mfgDate, expDate;
    private List<Category> categories;

    public Product() {
    }

    public Product(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getExpDate() {
        return expDate;
    }

    public Date getMfgDate() {
        return mfgDate;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public void setMfgDate(Date mfgDate) {
        this.mfgDate = mfgDate;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }


}

